# Magic Mirror

My Magic Mirror (in progress)

# Hardware

Most of the parts I already had lying around, so the main cost was the glass & frame.

|                                                  Thing                                                   |   Cost  |
|----------------------------------------------------------------------------------------------------------|---------|
| **[22.5" x 14" Dielectric Mirror - 1/8" Thickness](https://www.twowaymirrors.com/glass/)**               | $208.15 |
| **[22.5" x 14" Smart Mirror Frame](https://www.twowaymirrors.com/buy-smart-mirror-frame/)**              | $169.38 |
| **[24" Acer Monitor (S240HL)](https://www.newegg.com/p/N82E16824009739)**                                |     N/A |
| HDMI Cable                                                                                               |     N/A |


